Cache Interface
============

PHP interface library which provides an cache interface for several adapters.

Usage
-----

1) Using the ArrayCache (Memory Cache)

```php

// Using the array cache
$ws->setCache(new \TwoDevs\Cache\ArrayCache());

```

2) If doctrine cache is installed, it can be used to cache the result container using the doctrine cache adapter.

```php

// Create a new DoctrineCache instance
$doctrineCache = new \Doctrine\Common\Cache\FilesystemCache('var/cache');

// Create a new DoctrineCache adapter
$cacheAdapter = new \TwoDevs\Cache\DoctrineCache($doctrineCache);

```

3) If zend cache is installed, it can be used to cache the result container using the zend cache adapter.

```php

// Create a new ZendStorage instance
$zendCache = new \Zend\Cache\Storage\Adapter\Memory();

// Create a new ZendCache adapter
$cacheAdapter = new \TwoDevs\Cache\ZendCache($zendCache);

```
