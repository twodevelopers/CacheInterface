<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 19.02.2015
 * Time: 23:00
 */

namespace TwoDevs\Cache;

use Stash\Interfaces\PoolInterface;

class StashCache implements CacheInterface {

    private $cache;

    function __construct(PoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Fetches an entry from the cache.
     *
     * @param string $id The id of the cache entry to fetch.
     *
     * @return mixed The cached data or FALSE, if no cache entry exists for the given id.
     */
    public function fetch( $id ) {

        try {
            $item = $this->cache->getItem($id);
            return !$item->isMiss() ? $item->get() : false;
        } catch (\InvalidArgumentException $exp) {}

        return false;
    }

    /**
     * Tests if an entry exists in the cache.
     *
     * @param string $id The cache id of the entry to check for.
     *
     * @return boolean TRUE if a cache entry exists for the given cache id, FALSE otherwise.
     */
    public function contains( $id ) {
        try {
            return !$this->cache->getItem($id)->isMiss();
        } catch (\InvalidArgumentException $exp) {}

        return false;
    }

    /**
     * Puts data into the cache.
     *
     * @param string $id The cache id.
     * @param mixed $data The cache entry/data.
     * @param int $lifeTime The cache lifetime.
     *                         If != 0, sets a specific lifetime for this cache entry (0 => infinite lifeTime).
     *
     * @return boolean TRUE if the entry was successfully stored in the cache, FALSE otherwise.
     */
    public function save( $id, $data, $lifeTime = 0 ) {

        $lifeTime = $lifeTime > 0 ? $lifeTime : null;

        try {
            $this->cache->getItem($id)->set($data, $lifeTime);
            return true;
        } catch(\InvalidArgumentException $exp) { }

        return false;
    }

    /**
     * Deletes a cache entry.
     *
     * @param string $id The cache id.
     *
     * @return boolean TRUE if the cache entry was successfully deleted, FALSE otherwise.
     */
    public function delete( $id ) {
        try {
            return $this->cache->getItem($id)->clear();
        } catch(\InvalidArgumentException $exp) { }

        return false;
    }

    /**
     * Flushes all cache entries.
     *
     * @return boolean TRUE if the cache entries were successfully flushed, FALSE otherwise.
     */
    public function flushAll() {
        return $this->cache->flush();
    }
}
