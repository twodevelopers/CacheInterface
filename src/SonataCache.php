<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 03.02.2015
 * Time: 14:44
 */

namespace TwoDevs\Cache;

use Sonata\Cache\CacheAdapterInterface;
use Sonata\Cache\CacheElement;

class SonataCache implements CacheInterface {

    private $cache;

    function __construct(CacheAdapterInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Fetches an entry from the cache.
     *
     * @param string $id The id of the cache entry to fetch.
     *
     * @return mixed The cached data or FALSE, if no cache entry exists for the given id.
     */
    public function fetch( $id ) {
        $data = $this->cache->get(array('id' => $id))->getData();
        return !$data ? false : $data;
    }

    /**
     * Tests if an entry exists in the cache.
     *
     * @param string $id The cache id of the entry to check for.
     *
     * @return boolean TRUE if a cache entry exists for the given cache id, FALSE otherwise.
     */
    public function contains( $id ) {
        return $this->cache->has(array('id' => $id));
    }

    /**
     * Puts data into the cache.
     *
     * @param string $id The cache id.
     * @param mixed $data The cache entry/data.
     * @param int $lifeTime The cache lifetime.
     *                         If != 0, sets a specific lifetime for this cache entry (0 => infinite lifeTime).
     *
     * @return boolean TRUE if the entry was successfully stored in the cache, FALSE otherwise.
     */
    public function save( $id, $data, $lifeTime = 0 ) {

        if(0 === $lifeTime) {
            $lifeTime = CacheElement::MONTH;
        }

        $this->cache->set(array('id' => $id), $data, $lifeTime);
        return true;
    }

    /**
     * Deletes a cache entry.
     *
     * @param string $id The cache id.
     *
     * @return boolean TRUE if the cache entry was successfully deleted, FALSE otherwise.
     */
    public function delete( $id ) {
        return $this->cache->flush(array('id' => $id));
    }

    /**
     * Flushes all cache entries.
     *
     * @return boolean TRUE if the cache entries were successfully flushed, FALSE otherwise.
     */
    public function flushAll() {
        return $this->cache->flushAll();
    }
}
