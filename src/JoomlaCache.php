<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 03.02.2015
 * Time: 15:32
 */

namespace TwoDevs\Cache;

/**
 * Class JoomlaCache
 * @package TwoDevs\Cache
 * @deprecated Use PsrCompatible Cache instead
 */
class JoomlaCache extends PsrCompatibleCache { }
