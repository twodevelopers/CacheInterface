<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 03.02.2015
 * Time: 15:09
 */

namespace TwoDevs\Cache\Sonata;

use Sonata\Cache\Adapter\Cache\BaseCacheHandler;
use Sonata\Cache\CacheElement;

class SonataArrayCache extends  BaseCacheHandler {

    private $items = array();

    /**
     * Gets data from cache
     *
     * @param array $keys
     *
     * @return CacheElement
     */
    function get( array $keys ) {

        if($this->has($keys)) {
            $ckeys = $this->computeCacheKeys($keys);
            return $this->handleGet($keys, $this->items[$ckeys]);
        }

        return $this->handleGet($keys, null);
    }

    /**
     * Returns TRUE whether cache contains data identified by keys
     *
     * @param array $keys
     *
     * @return boolean
     */
    function has( array $keys ) {
        $ckeys = $this->computeCacheKeys($keys);
        return isset($this->items[$ckeys]);
    }

    /**
     * Sets value in cache
     *
     * @param array $keys An array of keys
     * @param mixed $value Value to store
     * @param integer $ttl A time to live, default 86400 seconds (CacheElement::DAY)
     * @param array $contextualKeys An array of contextual keys
     *
     * @return CacheElement
     */
    function set( array $keys, $value, $ttl = CacheElement::DAY, array $contextualKeys = array() ) {

        $value = new CacheElement($keys, $value, $ttl, $contextualKeys);
        $ckeys = $this->computeCacheKeys($keys);
        $this->items[$ckeys] = $value;

        return $value;
    }

    /**
     * Flushes data from cache identified by keys
     *
     * @param array $keys
     *
     * @return boolean
     */
    function flush( array $keys = array() ) {

        if($this->has($keys)) {
            $cKeys = $this->computeCacheKeys($keys);
            unset($this->items[$cKeys]);
            return true;
        }

        return false;
    }

    /**
     * Flushes all data from cache
     *
     * @return boolean
     */
    function flushAll() {
        $this->items = array();
        return true;
    }

    /**
     * Returns TRUE whether cache is contextual
     *
     * @return boolean
     */
    function isContextual() {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    private function computeCacheKeys(array $keys)
    {
        ksort($keys);
        return md5(serialize($keys));
    }
}
