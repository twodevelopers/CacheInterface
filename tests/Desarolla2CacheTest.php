<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 23:11
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

class Desarolla2CacheTest extends AbstractCacheTest {

    /** @return \TwoDevs\Cache\CacheInterface */
    protected function createCache()
    {
        $adapter = new \Desarrolla2\Cache\Adapter\Memory();
        $cache = new \Desarrolla2\Cache\Cache($adapter);
        return new \TwoDevs\Cache\Desarrolla2Cache($cache);
    }

    public function testFlushAll() {

        $cache = $this->createCache();
        $this->assertFalse($cache->flushAll());
    }
}
