<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 22:13
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

class ZendCacheTest extends AbstractCacheTest {

    protected function createCache() {

        $zendCache = new \Zend\Cache\Storage\Adapter\Memory();
        $zendCachePlugin = new \Zend\Cache\Storage\Plugin\ExceptionHandler();
        $zendCachePlugin->getOptions()->setThrowExceptions(false);
        $zendCache->addPlugin($zendCachePlugin);

        return new \TwoDevs\Cache\ZendCache($zendCache);
    }
}
