<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 22:13
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

class SonataCacheTest extends AbstractCacheTest {

    protected function createCache() {
        return new \TwoDevs\Cache\SonataCache(new \TwoDevs\Cache\Sonata\SonataArrayCache());
    }
}
