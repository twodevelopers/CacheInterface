<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 22:13
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

class DoctrineCacheTest extends AbstractCacheTest {

    protected function createCache() {
        return new \TwoDevs\Cache\DoctrineCache(new \Doctrine\Common\Cache\ArrayCache());
    }
}
