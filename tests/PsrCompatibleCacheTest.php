<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 22:13
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

class PsrCompatibleCacheTest extends AbstractCacheTest {

    protected function createCache() {
        $cache = new \TwoDevs\Cache\PsrCompatibleCache(new \Joomla\Cache\Runtime());
        $cache->flushAll();
        return $cache;
    }
}
