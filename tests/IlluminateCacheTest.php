<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 23:28
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

class IlluminateCacheTest extends AbstractCacheTest {

    /** @return \TwoDevs\Cache\CacheInterface */
    protected function createCache()
    {
        $cache = new \Illuminate\Cache\ArrayStore();
        return new \TwoDevs\Cache\IlluminateCache($cache);
    }
}
