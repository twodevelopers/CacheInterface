<?php
use TwoDevs\Cache\CacheInterface;

/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 22.01.2015
 * Time: 22:24
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

abstract class AbstractCacheTest extends PHPUnit_Framework_TestCase {

    /** @return CacheInterface */
    protected abstract function createCache();

    public function testSave() {

        $cache = $this->createCache();

        $this->assertTrue($cache->save('test', 'testdata'));
        $this->assertEquals('testdata', $cache->fetch('test'));
    }

    public function testFetch() {

        $cache = $this->createCache();

        $this->assertFalse($cache->fetch('test'));
        $this->assertTrue($cache->save('test', 'testdata'));
        $this->assertEquals('testdata', $cache->fetch('test'));
    }

    public function testContains() {

        $cache = $this->createCache();

        $this->assertFalse($cache->contains('test'));
        $this->assertTrue($cache->save('test', 'testdata'));
        $this->assertTrue($cache->contains('test'));
    }

    public function testDelete() {

        $cache = $this->createCache();

        $this->assertTrue($cache->save('test', 'testdata'));
        $this->assertTrue($cache->contains('test'));
        $this->assertTrue($cache->delete('test'));
        $this->assertFalse($cache->contains('test'));
    }

    public function testFlushAll() {

        $cache = $this->createCache();

        $this->assertTrue($cache->save('test', 'testdata'));
        $this->assertTrue($cache->contains('test'));
        $this->assertTrue($cache->flushAll());
        $this->assertFalse($cache->contains('test'));
    }
}
